//
//  RNReactNativeFlyshot.m
//
//  Created by Alexander Samusevich on 29/08/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTLog.h>
#import <StoreKit/StoreKit.h>

#import "RNReactNativeFlyshot.h"

@implementation RNReactNativeFlyshot

NSArray * blockedPurchases = nil;

- (NSArray<NSString *> *)supportedEvents
{
    return @[@"onFlyshotPurchase", @"onFlyshotCampaignDetected"];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [Flyshot shared].delegate = self;
    }
    return self;
}

+ (BOOL)requiresMainQueueSetup
{
    return YES;
}

#pragma mark - Flyshot Delegate

- (void)flyshotPurchaseWithPaymentQueue:(SKPaymentQueue *)paymentQueue updatedTransactions:(NSArray<SKPaymentTransaction *> *)transactions {

    RCTLogInfo(@"Flyshot SDK purchase delegate called");

    NSMutableArray *output = [NSMutableArray array];

    for (SKPaymentTransaction *item in transactions) {

        NSNumber *transactionDate = @(item.transactionDate.timeIntervalSince1970 * 1000);
        NSString *transactionIdentifier = item.transactionIdentifier;
        NSString *paymentIdentifier = item.payment.productIdentifier;

        NSMutableDictionary *purchase = [[NSMutableDictionary alloc] initWithCapacity:4];
        [purchase setValue:transactionDate forKey:@"transactionDate"];
        [purchase setValue:transactionIdentifier forKey:@"transactionId"];
        [purchase setValue:paymentIdentifier forKey:@"productId"];
        [purchase setValue:[NSNumber numberWithInt:item.transactionState] forKey:@"transactionState"];

        if (purchase) {
            [output addObject:purchase];
        }
    }

    [self sendEventWithName:@"onFlyshotPurchase"
                       body:@{@"transactions": output}];
}

- (BOOL)allowFlyshotPurchaseWithProductIdentifier:(NSString *)productIdentifier {

    RCTLogInfo(@"Flyshot SDK allowPurchase called with identifier %@", productIdentifier);

    if (blockedPurchases) {
        return ![blockedPurchases containsObject:productIdentifier];
    }
    return TRUE;
}

- (void)flyshotCampaignDetectedWithProductId:(NSString * _Nullable)productId {

    RCTLogInfo(@"Flyshot SDK campaignDetect called with product identifier %@", productId);

    [self sendEventWithName:@"onFlyshotCampaignDetected"
                       body:@{@"productId": productId != nil ? productId : [NSNull null]}];
}


#pragma mark - Exported methods

RCT_EXPORT_MODULE(Flyshot);


RCT_REMAP_METHOD(initialize,
                 sdkToken:(NSString *)token
                 initializeResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    RCTLogInfo(@"Flyshot SDK initialize with token %@s", token);

    [[Flyshot shared] initializeWithSdkToken:token onSuccess:^{
        resolve(NULL);
    } onFailure:^(NSError * _Nonnull error) {
        reject(NULL, NULL, error);
    }];
}


RCT_EXPORT_METHOD(start)
{
    RCTLogInfo(@"Flyshot SDK calls start");

    [[Flyshot shared] start];
}

RCT_REMAP_METHOD(setAppsFlyerId,
                 appsflyerId:(NSString *)appsflyerId)
{
    RCTLogInfo(@"Flyshot SDK setAppsFlyerId with appsflyerId %@s", appsflyerId);
    [Flyshot shared].appsflyerId = appsflyerId;
}

RCT_REMAP_METHOD(isFlyshotUser,
                  isFlyshotUserResolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
    BOOL result = [[Flyshot shared] isFlyshotUser];
    RCTLogInfo(@"Flyshot SDK isFlyshotUser called");

    resolve([NSNumber numberWithBool:result]);
}

RCT_REMAP_METHOD(isEligibleForDiscount,
                 isEligibleForDiscountResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    BOOL result = [[Flyshot shared] isEligibleForDiscount];
    RCTLogInfo(@"Flyshot SDK isEligibleForDiscount called");

    resolve([NSNumber numberWithBool:result]);
}

RCT_REMAP_METHOD(isEligibleForPromo,
                 isEligibleForPromoResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    BOOL result = [[Flyshot shared] isEligibleForPromo];
    RCTLogInfo(@"Flyshot SDK isEligibleForPromo called");

    resolve([NSNumber numberWithBool:result]);
}

RCT_REMAP_METHOD(isCampaignActive,
                 campaignProductId:(NSString * _Nullable)productId
                 isCampaignActiveResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    BOOL result = [[Flyshot shared] isCampaignActiveWithProductId:productId];
    RCTLogInfo(@"Flyshot SDK isCampaignActive called");

    resolve([NSNumber numberWithBool:result]);
}

RCT_REMAP_METHOD(isEligibleForReward,
                 isEligibleForRewardResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    BOOL result = [[Flyshot shared] isEligibleForReward];
    RCTLogInfo(@"Flyshot SDK isEligibleForReward called");

    resolve([NSNumber numberWithBool:result]);
}

RCT_EXPORT_METHOD(requestPromoBanner)
{
    RCTLogInfo(@"Flyshot SDK calls requestPromoBanner");

    [[Flyshot shared] requestPromoBanner];
}

RCT_REMAP_METHOD(sendConversionEvent,
                 conversionEventAmount:(double)amount
                 conversionEventProductId:(NSString * _Nullable)productId
                 currencyCode:(NSString * _Nullable)code
                 conversionEventResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    RCTLogInfo(@"Flyshot SDK sendConversionEvent with amount %f and productId %@s (currency %@s)",
               amount,
               productId,
               code
    );

    [[Flyshot shared] sendConversionEventWithAmount:amount currencyCode:code productId:productId onSuccess:^{
        resolve(NULL);
    } onFailure:^(NSError * _Nonnull error) {
        reject(NULL, NULL, error);
    }];
}

RCT_REMAP_METHOD(sendEvent,
                 eventName:(NSString *)name
                 eventResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    RCTLogInfo(@"Flyshot SDK sendCustomEvent called with name %@s", name);

    [[Flyshot shared] sendCustomEventWithName:name onSuccess:^{
        resolve(NULL);
    } onFailure:^(NSError * _Nonnull error) {
        reject(NULL, NULL, error);
    }];
}

RCT_REMAP_METHOD(sendSignUpEvent,
                 signUpEventResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    RCTLogInfo(@"Flyshot SDK sendSignUpEventWithComplete called");

    [[Flyshot shared] sendSignUpEventOnSuccess:^{
        resolve(NULL);
    } onFailure:^(NSError * _Nonnull error) {
        reject(NULL, NULL, error);
    }];
}

RCT_EXPORT_METHOD(setBlockedPurchasesList:(NSArray*)purchases)
{
    blockedPurchases = purchases;
}

RCT_REMAP_METHOD(upload,
                 uploadResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    RCTLogInfo(@"Flyshot SDK upload called");

    [[Flyshot shared] uploadOnSuccess:^(enum CampaignStatus status) {
        NSNumber *val = [NSNumber numberWithInteger:status];
        resolve(val);
    } onFailure:^(NSError * _Nonnull error) {
        reject(NULL, NULL, error);
    }];
}

RCT_REMAP_METHOD(getContent,
                 getContentResolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
    RCTLogInfo(@"Flyshot SDK getContent called");

    [[Flyshot shared] getContentOnSuccess:^(NSArray<Post *> * _Nonnull content) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            for (Post * post in content) {
                [dict setValue:post.creatorHandle forKey:@"creatorHandle"];
                [dict setValue:post.creatorName forKey:@"creatorName"];
                [dict setValue:post.caption forKey:@"caption"];
                [dict setValue:(post.imageURL ? post.imageURL.absoluteString : @"") forKey:@"imageURL"];
                [dict setValue:(post.thumbnailURL ? post.thumbnailURL.absoluteString : @"") forKey:@"thumbnailURL"];
                [dict setValue:(post.postLink ? post.postLink.absoluteString : @"") forKey:@"postLink"];
            }
            resolve(dict);
        } onFailure:^(NSError * _Nonnull error) {
            reject(NULL, NULL, error);
        }];
}


@end
