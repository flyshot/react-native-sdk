
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <FlyshotSDK/FlyshotSDK-Swift.h>

@interface RNReactNativeFlyshot : RCTEventEmitter <RCTBridgeModule, FlyshotDelegate>

@end
  
