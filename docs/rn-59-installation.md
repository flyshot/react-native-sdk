Install the SDK for RN < 0.61
------------------------------------------

Please check that you defined Swift version for your project target.
If you are already using Swift in your existing project skip this step.
Go to project target -> build settings -> Press the + button -> Select Add User-Defined Setting. 
Change new added row key to "SWIFT_VERSION" -> set "SWIFT_VERSION" value equal or more then minimum support Swift version (4)
 
#### Option 1: Cocoapods (Recommended)

Navigate to your project folder in a terminal window. Make sure you have the CocoaPods gem installed on your
machine before installing the Flyshot.
We recommend using latest version of React Native: 
       
```bash
$ sudo gem install cocoapods
$ pod init
```

### Link native dependencies

```bash
react-native link @flyshot/react-native-sdk
```

### Check and install new pod

This will create a file named Podfile in your project's root directory.
Add `pod 'Flyshot'` to your Podfile. Minimal platform version should be `10.0`:
     
```
platform :ios, '10.0'

target 'FlyshotDemo' do
  
  pod 'Flyshot'
  
end
``` 
    
Run the following command in your project root directory from a terminal window:
    
```bash
$ pod install
```
    
#### Option 2: Manual way

### Link native dependencies

Manually Link the library in Xcode ([Linking libraries on iOS](https://facebook.github.io/react-native/docs/linking-libraries-ios.html))

1. Open Xcode -> Right click "[Your Project Name]/Libraries" folder and select "Add File to [Your Project Name]" -> Select `RNReactNativeFlyshot.xcodeproj` located in `node_modules/@flyshot/react-native-sdk/ios`.
2. Open "General Settings" -> "Build Phases" -> "Link Binary with Libraries" and add `libRNReactNativeFlyshot.a`

### Link native framework

To integrate Flyshot SDK via xcframeworks follow next steps:

1. Clone [SDK repository](https://bitbucket.org/flyshot/ios-sdk.git) in separate folder

2. Extract FlyshotSDK.xcframework.zip file

3. In XCode, select your project in Project navigator

4. Finder, navigate to where you extracted the SDK

5. Drag FlyshotSDK.xcframework folder to the XCode project root (into the project navigator)

6. Set the Add Files options as follows:

   
       Destination - select Copy items if needed
       
       Added folders - select Create groups
   
7. Check "Frameworks and Libraries" section under the general settings tab of your application's target 
and set the Embed dropdown as "Embed and Sign" for Flyshot.framework.

![Process diagram](images/img1.jpg)
    
Finally, reconfigure search path. Select Library/RNReactNativeFlyshot.xcproject in the project navigator and select target. 
Next open Build Settings tab and search "Search path" section. 
Edit "Framework Search Path" to add "$(SRCROOT)/../../../../ios/FlyshotSDK.xcframework" path.
Note: path must be recursive.    

![Process diagram](images/img2.jpg)
