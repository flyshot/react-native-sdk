Usage
------------------------------------------

### Import or Require the module

```javascript
import Flyshot from '@flyshot/react-native-sdk';
```

or

```javascript
var Flyshot = require('@flyshot/react-native-sdk');
```

### SDK initialization

To initialize SDK call `initialize` method with token generated in your Flyshot account:

```javascript
const sdkToken = '...';
Flyshot.initialize(sdkToken): Promise
```

To start SDK call `start` method:

```javascript
Flyshot.start();
```


Are you using AppsFlyer to track events? Just use `setAppsFlyerId` method to set AppsFlyer Id:

```javascript
const appsflyerId = '...';
Flyshot.setAppsFlyerId(appsflyerId);
``` 

### Public interface

Check user_id session variable. Return true if Flyshot user_id session variable is set:

```javascript
Flyshot.isFlyshotUser(): Promise
```

Check active campaign session variable. Return true if Flyshot active campaign session variable is set:

```javascript
Flyshot.isEligibleForDiscount(): Promise
```

```javascript
Flyshot.isEligibleForPromo(): Promise
```

```javascript
Flyshot.isEligibleForReward(): Promise
```

Check if campaign is active:

```javascript
const productId = '...';
Flyshot.isCampaignActive(productId): Promise
```

For example:

```
export default class ExampleStateView extends Component {

  constructor(props) {
    super(props);
    state = {
      isFlyshotUser: false,
      isEligibleForDiscount: false,
    }
  }
  
  checkState = async () => {
  
      const isFlyshotUser = await Flyshot.isFlyshotUser();
      const isEligibleForDiscount = await Flyshot.isEligibleForDiscount();
      const isEligibleForPromo = await Flyshot.isEligibleForPromo();
      const isEligibleForReward = await Flyshot.isEligibleForReward();
    
      /**
       * // Or run both methods in parallel
       *
       * const [isFlyshotUser, isEligibleForDiscount] = await Promise.all([
       *     Flyshot.isFlyshotUser(),
       *     Flyshot.isEligibleForDiscount(),
       *     Flyshot.isEligibleForPromo(),
       *     Flyshot.isEligibleForReward(),
       * ]);
      */
    
      this.setState({
          isFlyshotUser,
          isEligibleForDiscount,
          isEligibleForPromo,
          isEligibleForReward,
      });
  }
}

```

Send Checkout Event to Flyshot Analytics:

```javascript
Flyshot.sendConversionEvent(amount, ?productId, ?currency): Promise
```
Note: You can pass `null` instead of `productId` and pass `null` instead of currency (USD will be used)

Send Custom Event to Flyshot Analytics:

```javascript
Flyshot.sendEvent(eventName): Promise
```

Send Sign Up Event to Flyshot Analytics:

```javascript
Flyshot.sendSignUpEvent(): Promise
```

Set list of product ids which should not processed:

```javascript
Flyshot.setBlockedPurchasesList(Array<String>): Void
```

Open promo board manually:

```javascript
Flyshot.requestPromoBanner(): Void
```

Upload screenshot (response will return campaign search status: 0 - not found, 1 - found, 2 - redeemed):

```javascript
Flyshot.upload(): Promise
```

Get available content for your application:

```javascript
Flyshot.getContent(): Promise
```

### Events

You should use a NativeEmitter to subscribe Flyshot events:

```javascript
import {NativeEventEmitter} from 'react-native';

// ...

const flyshotManagerEmitter = new NativeEventEmitter(Flyshot);

flyshotManagerEmitter.addListener(
    'onFlyshotPurchase',
    (transactions) => {
        /*
         * transactions = [
         *   {
         *      transactionDate: timestamp,
         *      transactionId: string,
         *      productId: string,
         *      transactionState: int,  
         *    },
         *    ...
         * ]
        */
    }
);

flyshotManagerEmitter.addListener(
    'onFlyshotCampaignDetected',
    (event) => {
       /*
        * event = {
        *     productId: string,
        * }
        *  
        */
    }
);
```

## Universal links and deep links processing

Follow react native documentation for [Linking](https://facebook.github.io/react-native/docs/linking) component
to process deep links and universal links in your app.
