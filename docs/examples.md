Examples
----------------------------------------

### Working with Flyshot Checkout Event

If you have an internal payment system (such as Stripe or PayPal), you have the ability to promote your products by 
providing a discount for users who came as a result of Flyshot campaign.

After the initialization, Flyshot can identify automatically if user is eligible for discount or not.

In your code, you can check through Flyshot Public API if user is eligible for a discount, and then apply your desired 
logic in code that is responsible for price calculation. See example below:
 
```
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import Flyshot from "@flyshot/react-native-sdk";

export default class Cart extends Component {

    constructor() {
        super();

        this.discount = 20; // in this case Flyshot user gets 20% off
        this.taxRate = 13; // in this case 13% tax rate

        this.state = {
            total: 0,
        };

        this.calculatePrice().then((total) => {
            this.setState({
                total,
            });
        });
    }

    // This method should calculate the price before showing in the Checkout cart
    async calculatePrice() {
        const originalPrice = 49.99; // Your original checkout cart price here
        const isEligibleForDiscount = await Flyshot.isEligibleForDiscount();
        const subtotal = isEligibleForDiscount ? originalPrice * (100 - this.discount) / 100 : originalPrice;
        const tax = subtotal / 100 * this.taxRate;
        const total = subtotal + tax;

        return total;
    }

    // This method should send a Checkout event to Flyshot Dashboard after successful payment
    submitPayment() {
        this.calculatePrice().then((amount) => {

            // Your logic here for submitting the payment
            // If the payment was successfull, add the code below

            Flyshot.sendConversionEvent(null, amount, currency)
                .then(() => {
                    // Success callback handler
                })
                .catch((error) => {
                    // Error callback handler
                    console.log(error);
                });
        });
    }

    render() {
        const {total} = this.state;
        return (
            <View>
                <Text>Your cart total: {total} CAD</Text>
            </View>
        );
    }
}

```
