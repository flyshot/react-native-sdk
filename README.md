# react-native-sdk

React Native wrapper for Flyshot SDK. This wrapper based on the native [IOS Flyshot SDK](https://bitbucket.org/flyshot/ios-sdk)

Installation Guide
---------------------------------

### Install for iOS:

### Install `react-native-sdk`:

```bash
yarn add @flyshot/react-native-sdk  # or npm install @flyshot/react-native-sdk
```

Note: Do not forget run `pod update` after package installation.

If you are using old version of React Native (< 0.61) you should use 
deprecated way to install dependencies. Please check [this article](docs/rn-59-installation.md) to install 
Flyshot RN SDK for an old version of React Native.

Next please check other documentation articles:

[Usage documentation](docs/usage.md)

[Usage Examples](docs/examples.md)

[Licenses](docs/licenses.md)
